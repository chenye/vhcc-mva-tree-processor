# VHcc MVA tree processor

- source :
    - Nominal : use to generate Hists from MVA tree
    - FileSlim (use this) : use to generate small ntuple with BVeto & TX requirments 
    - JetReRank (not recommended): use to generate small ntuple with BVeto & TX requirments 
    - ThirdJet : small check for third Jet 
- run : 
    - C++ for running source code : root runXXX.C
