#define JetReRank_cxx
#include "Function.C"
#include "JetReRank.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>

void JetReRank::Loop(string outputName, bool slimNtuple, bool ApplyReRank = true, bool ApplyPtRank = false , bool ApplyDRCut = false)
{
    //   In a ROOT session, you can do:
    //      root> .L JetReRank.C
    //      root> JetReRank t
    //      root> t.GetEntry(12); // Fill t data members with entry number 12
    //      root> t.Show();       // Show values of entry 12
    //      root> t.Show(16);     // Read and show values of entry 16
    //      root> t.Loop();       // Loop on all entries
    //

    //     This is the loop skeleton where:
    //    jentry is the global entry number in the chain
    //    ientry is the entry number in the current Tree
    //  Note that the argument to GetEntry must be:
    //    jentry for TChain::GetEntry
    //    ientry for TTree::GetEntry and TBranch::GetEntry
    //
    //       To read only selected branches, Insert statements like:
    // METHOD1:
    //    fChain->SetBranchStatus("*",0);  // disable all branches
    //    fChain->SetBranchStatus("branchname",1);  // activate branchname
    // METHOD2: replace line
    //    fChain->GetEntry(jentry);       //read all branches
    //by  b_branchname->GetEntry(ientry); //read only this branch
    if (fChain == 0) return;
    //define New variable
    Float_t   m_dPhiVBB, m_dEtaVBB,m_dRVBB;
    Float_t   m_dRBB,m_dPhiBB,m_dEtaBB,m_mBB;
    Float_t   m_pTB1,m_pTB2, m_pTJ3;
    Float_t   m_etaB1, m_etaB2, m_etaJ3;
    Float_t   m_phiB1, m_phiB2, m_phiJ3;
    Float_t   m_mB1, m_mB2, m_mJ3;
    Float_t   m_bin_bTagB1, m_bin_bTagB2,  m_bin_bTagJ3;
    Float_t   m_DL1r_pb_B1, m_DL1r_pc_B1,  m_DL1r_pu_B1;
    Float_t   m_DL1r_pb_B2, m_DL1r_pc_B2,  m_DL1r_pu_B2;
    Float_t   m_DL1r_pb_J3, m_DL1r_pc_J3, m_DL1r_pu_J3;
    Int_t     m_FlavB1,   m_FlavB2,    m_FlavJ3;

    // Define new tree in the root file : 
    TFile f_new(outputName.c_str(),"recreate");
    TTree *MVATree = new TTree("Nominal","Nominal");
    MVATree->Branch("sample",        &sample        );
    MVATree->Branch("EventWeight",   &EventWeight   );
    MVATree->Branch("bTagWeight",    &bTagWeight    );
    MVATree->Branch("EventNumber",   &EventNumber   );
    MVATree->Branch("ChannelNumber", &ChannelNumber );
    MVATree->Branch("EventFlavor",   &EventFlavor   );
    MVATree->Branch("FlavourLabel",  &FlavourLabel  );
    MVATree->Branch("Description",   &Description   );
    MVATree->Branch("isResolved",    &isResolved    );
    MVATree->Branch("isBoosted",     &isBoosted     );
    MVATree->Branch("nJ",            &nJ            );
    MVATree->Branch("nSigJet",       &nSigJet       );
    MVATree->Branch("nForwardJet",   &nForwardJet   );
    MVATree->Branch("nTags",         &nTags         );
    MVATree->Branch("nTaus",         &nTaus         );
    MVATree->Branch("MET",           &MET           );
    MVATree->Branch("MEff",          &MEff          );
    MVATree->Branch("MEffBoosted", &MEffBoosted     );
    MVATree->Branch("METSig", &METSig         );
    MVATree->Branch("sumPtJets", &sumPtJets   );
    MVATree->Branch("softMET", &softMET       );
    MVATree->Branch("hasFSR", &hasFSR         );
    MVATree->Branch("nFatJets", &nFatJets     );
    MVATree->Branch("NMatchedTrackJetLeadFatJet", &NMatchedTrackJetLeadFatJet            );
    MVATree->Branch("NBTagMatchedTrackJetLeadFatJet", &NBTagMatchedTrackJetLeadFatJet    );
    MVATree->Branch("NBTagUnmatchedTrackJetLeadFatJet", &NBTagUnmatchedTrackJetLeadFatJet);
    MVATree->Branch("NAdditionalCaloJets", &NAdditionalCaloJets                          );
    MVATree->Branch("pTAddCaloJets", &pTAddCaloJets  );
    MVATree->Branch("deltaYVJ", &deltaYVJ            );
    MVATree->Branch("absdeltaPhiVJ", &absdeltaPhiVJ  );
    MVATree->Branch("HTBoosted", &HTBoosted          );
    MVATree->Branch("pTV",       &pTV    );
    MVATree->Branch("phiV",      &phiV   );
    MVATree->Branch("mJ",        &mJ     );
    MVATree->Branch("pTJ",       &pTJ    );
    MVATree->Branch("deltaRbTrkJbTrkJ", &deltaRbTrkJbTrkJ);
    MVATree->Branch("D2", &D2);
    MVATree->Branch("C2", &C2);
    MVATree->Branch("deltaRFJTrkJ1", &deltaRFJTrkJ1);
    MVATree->Branch("deltaRFJTrkJ2", &deltaRFJTrkJ2);
    MVATree->Branch("deltaRFJTrkJ3", &deltaRFJTrkJ3);
    MVATree->Branch("deltaRbTrkJ1TrkJ3", &deltaRbTrkJ1TrkJ3);
    //lepton 
    MVATree->Branch("FlavL1", &FlavL1);
    MVATree->Branch("FlavL2", &FlavL2);
    MVATree->Branch("mLL", &mLL);
    MVATree->Branch("etaV", &etaV);
    MVATree->Branch("pTL1", &pTL1);
    MVATree->Branch("pTL2", &pTL2);
    MVATree->Branch("etaL1", &etaL1);
    MVATree->Branch("etaL2", &etaL2);
    MVATree->Branch("phiL1", &phiL1);
    MVATree->Branch("phiL2", &phiL2);
    MVATree->Branch("cosThetaLep", &cosThetaLep);
    MVATree->Branch("lepPtBalance", &lepPtBalance);
    // BDT 
    MVATree->Branch("BDT", &BDT);
    MVATree->Branch("BDT_VZ", &BDT_VZ);
    MVATree->Branch("BDTBoosted", &BDTBoosted);
    //  Track Jets 1/2/3
    MVATree->Branch("pTBTrkJ1", &pTBTrkJ1    );
    MVATree->Branch("pTBTrkJ2", &pTBTrkJ2    );
    MVATree->Branch("pTBTrkJ3", &pTBTrkJ3    );
    MVATree->Branch("etaBTrkJ1", &etaBTrkJ1  );
    MVATree->Branch("etaBTrkJ2", &etaBTrkJ2);
    MVATree->Branch("etaBTrkJ3", &etaBTrkJ3);
    MVATree->Branch("phiBTrkJ1", &phiBTrkJ1);
    MVATree->Branch("phiBTrkJ2", &phiBTrkJ2);
    MVATree->Branch("phiBTrkJ3", &phiBTrkJ3);
    MVATree->Branch("bin_bTagBTrkJ1", &bin_bTagBTrkJ1);
    MVATree->Branch("bin_bTagBTrkJ2", &bin_bTagBTrkJ2);
    MVATree->Branch("bin_bTagBTrkJ3", &bin_bTagBTrkJ3);
    MVATree->Branch("DL1r_pb_TrkJ1", &DL1r_pb_TrkJ1);
    MVATree->Branch("DL1r_pc_TrkJ1", &DL1r_pc_TrkJ1);
    MVATree->Branch("DL1r_pu_TrkJ1", &DL1r_pu_TrkJ1);
    MVATree->Branch("DL1r_pb_TrkJ2", &DL1r_pb_TrkJ2);
    MVATree->Branch("DL1r_pc_TrkJ2", &DL1r_pc_TrkJ2);
    MVATree->Branch("DL1r_pu_TrkJ2", &DL1r_pu_TrkJ2);
    MVATree->Branch("DL1r_pb_TrkJ3", &DL1r_pb_TrkJ3);
    MVATree->Branch("DL1r_pc_TrkJ3", &DL1r_pc_TrkJ3);
    MVATree->Branch("DL1r_pu_TrkJ3", &DL1r_pu_TrkJ3);
    MVATree->Branch("Xbb202006", &Xbb202006);
    MVATree->Branch("nTagJet_4BVeto", &nTagJet_4BVeto);
    MVATree->Branch("FlavTrkJ1", &FlavTrkJ1);
    MVATree->Branch("FlavTrkJ2", &FlavTrkJ2);
    MVATree->Branch("FlavTrkJ3", &FlavTrkJ3);
    //Jet 1/2/3
    MVATree->Branch("pTB1", &m_pTB1);
    MVATree->Branch("pTB2", &m_pTB2);
    MVATree->Branch("pTJ3", &m_pTJ3);
    MVATree->Branch("etaB1", &m_etaB1);
    MVATree->Branch("etaB2", &m_etaB2);
    MVATree->Branch("etaJ3", &m_etaJ3);
    MVATree->Branch("phiB1", &m_phiB1);
    MVATree->Branch("phiB2", &m_phiB2);
    MVATree->Branch("phiJ3", &m_phiJ3);
    MVATree->Branch("mB1", &m_mB1);
    MVATree->Branch("mB2", &m_mB2);
    MVATree->Branch("mJ3", &m_mJ3);
    MVATree->Branch("bin_bTagB1", &m_bin_bTagB1);
    MVATree->Branch("bin_bTagB2", &m_bin_bTagB2);
    MVATree->Branch("bin_bTagJ3", &m_bin_bTagJ3);
    MVATree->Branch("DL1r_pb_B1", &m_DL1r_pb_B1);
    MVATree->Branch("DL1r_pc_B1", &m_DL1r_pc_B1);
    MVATree->Branch("DL1r_pu_B1", &m_DL1r_pu_B1);
    MVATree->Branch("DL1r_pb_B2", &m_DL1r_pb_B2);
    MVATree->Branch("DL1r_pc_B2", &m_DL1r_pc_B2);
    MVATree->Branch("DL1r_pu_B2", &m_DL1r_pu_B2);
    MVATree->Branch("DL1r_pb_J3", &m_DL1r_pb_J3);
    MVATree->Branch("DL1r_pc_J3", &m_DL1r_pc_J3);
    MVATree->Branch("DL1r_pu_J3", &m_DL1r_pu_J3);
    MVATree->Branch("FlavB1", &m_FlavB1);
    MVATree->Branch("FlavB2", &m_FlavB2);
    MVATree->Branch("FlavJ3", &m_FlavJ3);
    // Higgs candidate : BB 
    MVATree->Branch("dPhiBB",    &m_dPhiBB );
    MVATree->Branch("dEtaBB",    &m_dEtaBB );
    MVATree->Branch("dRBB",    &m_dRBB );
    MVATree->Branch("mBB",       &m_mBB    );
    MVATree->Branch("mBBJ",      &mBBJ   );
    MVATree->Branch("dPhiVBB", &m_dPhiVBB       );
    MVATree->Branch("dEtaVBB", &m_dEtaVBB       );
    MVATree->Branch("dRVBB", &m_dRVBB       );

    Long64_t nentries = fChain->GetEntriesFast();
    ///////////////////////////////////////////////////////
    // Histograms 
    ///////////////////////////////////////////////////////
    TH1D * h_cutFlow_2J = new TH1D("h_cutFlow_2J", "CutFlow for VHcc2Lep 2J region", 5,0,5);
    TH1D * h_cutFlow_3J = new TH1D("h_cutFlow_3J", "CutFlow for VHcc2Lep 3J region",5,0,5);
    TH1D * h_cutFlow = new TH1D("h_cutFlow", "CutFlow for VHcc2Lep", 5,0,5);
    TH1D * h_cutFlow_2J_weight = new TH1D("h_cutFlow_2J_weight", "CutFlow for VHcc2Lep 2J region (with event weight)", 5,0,5);
    TH1D * h_cutFlow_3J_weight = new TH1D("h_cutFlow_3J_weight", "CutFlow for VHcc2Lep 3J region (with event weight)",5,0,5);
    TH1D * h_cutFlow_weight = new TH1D("h_cutFlow_weight", "CutFlow for VHcc2Lep (with Event weight)", 5,0,5);
    TH2D * h_CtagCheck = new TH2D("h_CtagCheck", "CTag Check for CtagBins", 4,-0.5,3.5,4,-0.5,3.5);
    TH2D * h_CtagCheck_JetEta = new TH2D("h_CtagCheck_JetEta", "Eta_{Jet} Check for CtagBins", 60,-3,3, 4,-0.5,3.5);
    TH2D * h_CtagCheck_JetEtaB1 = new TH2D("h_CtagCheck_JetEtaB1", "Eta_{JetB1} Check for CtagBins", 60,-3,3, 4,-0.5,3.5);
    TH2D * h_CtagCheck_JetEtaB2 = new TH2D("h_CtagCheck_JetEtaB2", "Eta_{JetB2} Check for CtagBins", 60,-3,3, 4,-0.5,3.5);
    TH2D * h_CtagCheck_JetEtaJ3 = new TH2D("h_CtagCheck_JetEtaJ3", "Eta_{JetJ3} Check for CtagBins", 60,-3,3, 4,-0.5,3.5);

    TH2D * h_CtagCheck_JetpTB1 = new TH2D("h_CtagCheck_JetpTB1", "pT_{JetB1} Check for CtagBins", 55 ,0,550, 4,-0.5,3.5);
    TH2D * h_CtagCheck_JetpTB2 = new TH2D("h_CtagCheck_JetpTB2", "pT_{JetB2} Check for CtagBins", 55 ,0,550, 4,-0.5,3.5);
    TH2D * h_CtagCheck_JetpTJ3 = new TH2D("h_CtagCheck_JetpTJ3", "pT_{JetJ3} Check for CtagBins", 55 ,0,550, 4,-0.5,3.5);
    TH2D * h_CtagCheck_JetNum = new TH2D("h_CtagCheck_Num", "Number of Jet Check for CtagBins", 3,0.5,3.5, 4,-0.5,3.5);
    char* binLabel[5]= {"All","SF Lepton", "BVeto","#geq1CTagTight","dRBB Cut"};


    TH2D * h_nJnSigJ = new TH2D("h_nJnSigJ", "n_{Jets} vs n_{signalJets}", 3,2,5,3,2,5);

    h_nJnSigJ->GetXaxis()->SetTitle("n_{signalJet}");
    h_nJnSigJ->GetYaxis()->SetTitle("n_{Jet}");

    h_CtagCheck->GetXaxis()->SetBinLabel(1,"NoTag");
    h_CtagCheck->GetXaxis()->SetBinLabel(2,"CTagLoose");
    h_CtagCheck->GetXaxis()->SetBinLabel(3,"CTagTight");
    h_CtagCheck->GetXaxis()->SetBinLabel(4,"BTag");

    h_CtagCheck->GetYaxis()->SetBinLabel(1,"NoTag");
    h_CtagCheck->GetYaxis()->SetBinLabel(2,"CTagLoose");
    h_CtagCheck->GetYaxis()->SetBinLabel(3,"CTagTight");
    h_CtagCheck->GetYaxis()->SetBinLabel(4,"BTag");
    h_CtagCheck->GetYaxis()->SetTitle("CTagBin by Calculation");
    h_CtagCheck->GetXaxis()->SetTitle("CTagBin in PCFT");

    h_CtagCheck_JetpTJ3->GetYaxis()->SetBinLabel(1,"NoTag");
    h_CtagCheck_JetpTJ3->GetYaxis()->SetBinLabel(2,"CTagLoose");
    h_CtagCheck_JetpTJ3->GetYaxis()->SetBinLabel(3,"CTagTight");
    h_CtagCheck_JetpTJ3->GetYaxis()->SetBinLabel(4,"BTag");
    h_CtagCheck_JetpTJ3->GetYaxis()->SetTitle("CTagBin by Calculation");
    h_CtagCheck_JetpTJ3->GetXaxis()->SetTitle("pT_{J3}");


    h_CtagCheck_JetEtaJ3->GetYaxis()->SetBinLabel(1,"NoTag");
    h_CtagCheck_JetEtaJ3->GetYaxis()->SetBinLabel(2,"CTagLoose");
    h_CtagCheck_JetEtaJ3->GetYaxis()->SetBinLabel(3,"CTagTight");
    h_CtagCheck_JetEtaJ3->GetYaxis()->SetBinLabel(4,"BTag");
    h_CtagCheck_JetEtaJ3->GetYaxis()->SetTitle("CTagBin by Calculation");
    h_CtagCheck_JetEtaJ3->GetXaxis()->SetTitle("Eta_{J3}");

    h_CtagCheck_JetNum->GetYaxis()->SetBinLabel(1,"NoTag");
    h_CtagCheck_JetNum->GetYaxis()->SetBinLabel(2,"CTagLoose");
    h_CtagCheck_JetNum->GetYaxis()->SetBinLabel(3,"CTagTight");
    h_CtagCheck_JetNum->GetYaxis()->SetBinLabel(4,"BTag");
    h_CtagCheck_JetNum->GetYaxis()->SetTitle("CTagBin by Calculation");
    h_CtagCheck_JetNum->GetXaxis()->SetTitle("Jet Num.");



    for (int bin=0;bin<5;bin ++){
        h_cutFlow->GetXaxis()->SetBinLabel(bin+1,binLabel[bin]);
        h_cutFlow_2J->GetXaxis()->SetBinLabel(bin+1,binLabel[bin]);
        h_cutFlow_3J->GetXaxis()->SetBinLabel(bin+1,binLabel[bin]);
        h_cutFlow_weight->GetXaxis()->SetBinLabel(bin+1,binLabel[bin]);
        h_cutFlow_2J_weight->GetXaxis()->SetBinLabel(bin+1,binLabel[bin]);
        h_cutFlow_3J_weight->GetXaxis()->SetBinLabel(bin+1,binLabel[bin]);
    }
    Long64_t nbytes = 0, nb = 0;
    int counter=0;
    for (Long64_t jentry=0; jentry<nentries;jentry++) {
        Long64_t ientry = LoadTree(jentry);
        if (ientry < 0) break;
        nb = fChain->GetEntry(jentry);   nbytes += nb;
        //    if (ientry<3000000) continue;
        // Cuts 


        int PassSelection_Slim, PassSelection_dR , PassSelection_SF , PassSelection_BVeto, PassSelection_Previous=1;

        h_cutFlow->Fill(0.5);
        h_cutFlow_weight->Fill(0.5,EventWeight/bTagWeight);
        if (nJ<3) {
            h_cutFlow_2J->Fill(0.5);
            h_cutFlow_2J_weight->Fill(0.5,EventWeight/bTagWeight);
        }else {
            h_cutFlow_3J->Fill(0.5);
            h_cutFlow_3J_weight->Fill(0.5,EventWeight/bTagWeight);
        }
        //////////////////////////////////////////////
        //  Apply Same Flavor Cut 
        /////////////////////////
        //            std::cout <<"SF Lep :"<<*sample <<" Flav1 "<<FlavL1 <<" Falv2 "<<FlavL2<<std::endl;     
        PassSelection_SF = 1; 
        if (abs(FlavL1) != abs(FlavL2)) PassSelection_SF = 0;
        PassSelection_Previous *= PassSelection_SF;
        if (PassSelection_Previous){
            h_cutFlow->Fill(1.5);
            h_cutFlow_weight->Fill(1.5,EventWeight/bTagWeight);
            if (nJ<3) {
                h_cutFlow_2J->Fill(1.5);
                h_cutFlow_2J_weight->Fill(1.5,EventWeight/bTagWeight);
            }else {
                h_cutFlow_3J->Fill(1.5);
                h_cutFlow_3J_weight->Fill(1.5,EventWeight/bTagWeight);
            }

        }
        if (PassSelection_Previous){
            int m_ctagB1Cal=CTagBinsCal(DL1r_pb_B1,DL1r_pc_B1,DL1r_pu_B1);
            int m_ctagB2Cal=CTagBinsCal(DL1r_pb_B2,DL1r_pc_B2,DL1r_pu_B2);
            int m_ctagJ3Cal=CTagBinsCal(DL1r_pb_J3,DL1r_pc_J3,DL1r_pu_J3);
             
            if (bin_bTagB1==4)h_CtagCheck->Fill(3, m_ctagB1Cal);
            else h_CtagCheck->Fill(bin_bTagB1, m_ctagB1Cal);

            if (bin_bTagB2==4)h_CtagCheck->Fill(3, m_ctagB2Cal);
            else h_CtagCheck->Fill(bin_bTagB2, m_ctagB2Cal);
            
            if (bin_bTagB1==0)h_CtagCheck_JetEta->Fill(etaB1, m_ctagB1Cal);
            if (bin_bTagB2==0)h_CtagCheck_JetEta->Fill(etaB2, m_ctagB2Cal);

            if (bin_bTagB1==0)h_CtagCheck_JetEtaB1->Fill(etaB1, m_ctagB1Cal);
            if (bin_bTagB2==0)h_CtagCheck_JetEtaB2->Fill(etaB2, m_ctagB2Cal);
            if (bin_bTagB1==0)h_CtagCheck_JetpTB1->Fill(pTB1, m_ctagB1Cal);
            if (bin_bTagB2==0)h_CtagCheck_JetpTB2->Fill(pTB2, m_ctagB2Cal);
            
            if (bin_bTagB1==0)h_CtagCheck_JetNum->Fill(1, m_ctagB1Cal);
            if (bin_bTagB2==0)h_CtagCheck_JetNum->Fill(2, m_ctagB2Cal);
            
            if (nSigJet>2){
            //if (nJ>2){
                if (bin_bTagJ3==4)h_CtagCheck->Fill(3, m_ctagJ3Cal);
                else h_CtagCheck->Fill(bin_bTagJ3, m_ctagJ3Cal);
                if (bin_bTagJ3==0)h_CtagCheck_JetEta->Fill(etaJ3, m_ctagJ3Cal);
                if (bin_bTagJ3==0)h_CtagCheck_JetEtaJ3->Fill(etaJ3, m_ctagJ3Cal);
                if (bin_bTagJ3==0)h_CtagCheck_JetpTJ3->Fill(pTJ3, m_ctagJ3Cal);
                if (bin_bTagJ3==0)h_CtagCheck_JetNum->Fill(3, m_ctagJ3Cal);
            }

        }
        //////////////////////////////////////////////
        //  Apply BVeto  Cut 
        /////////////////////////
        PassSelection_BVeto = 1; 
        if (bin_bTagB1>2) PassSelection_BVeto = 0;
        if (bin_bTagB2>2) PassSelection_BVeto = 0 ;
        if (bin_bTagJ3>2 && nJ>2) PassSelection_BVeto = 0 ;

        PassSelection_Previous *= PassSelection_BVeto;
        if (PassSelection_Previous){
            h_cutFlow->Fill(2.5);
            h_cutFlow_weight->Fill(2.5,EventWeight/bTagWeight);
            if (nJ<3) {
                h_cutFlow_2J->Fill(2.5);
                h_cutFlow_2J_weight->Fill(2.5,EventWeight/bTagWeight);
            }else {
                h_cutFlow_3J->Fill(2.5);
                h_cutFlow_3J_weight->Fill(2.5,EventWeight/bTagWeight);
            }
        }
        //////////////////////////////////////////////
        // Slim Ntuple
        /////////////////////////
        PassSelection_Slim = 0;  
        if (nJ<3){ 
            if (bin_bTagB1==2 || bin_bTagB2 == 2 ) PassSelection_Slim = 1;
        }else{
            if (bin_bTagB1==2 || bin_bTagB2==2 || bin_bTagJ3 == 2 ) PassSelection_Slim = 1;
        }
        PassSelection_Previous *= PassSelection_Slim;
        if (PassSelection_Previous) {
            h_cutFlow->Fill(3.5);
            int m_nJ=nJ, m_nSJ=nSigJet;
            if (nSigJet > 4) m_nSJ=4;
            if (nJ >4 )m_nJ=4;
            h_nJnSigJ->Fill(m_nSJ, m_nJ,EventWeight/bTagWeight);

            h_cutFlow_weight->Fill(3.5,EventWeight/bTagWeight);
            if (nJ<3) {
                h_cutFlow_2J->Fill(3.5);
                h_cutFlow_2J_weight->Fill(3.5,EventWeight/bTagWeight);
            }else {
                h_cutFlow_3J->Fill(3.5);
                h_cutFlow_3J_weight->Fill(3.5,EventWeight/bTagWeight);
            }
        }
        //////////////////////////////////////////////
        // ReRank Jet 
        /////////////////////////
        int jetRank[3]={0,1,2};
        TLorentzVector m_Jet[2],m_H,m_V;
        float jetTag[3]={bin_bTagB1,bin_bTagB2,bin_bTagJ3};
        if (counter %100000 == 0) 
        {   
            std::cout<<"============ "<<*sample<<" Entry"<<counter<<" \t nJets "<<nJ<<" \t nSigJets" <<nSigJet<<"============="<<std::endl;
            std::cout<<" -------- Before rank -------------"<<std::endl;
            std::cout<<"Before"<< " \t Jet"<<jetRank[0]<<" \t Jet" <<jetRank[1]<<" \t Jet"<<jetRank[2]<<std::endl;
            std::cout<<"Before"<< " \t ctagbin"<<jetTag[jetRank[0]]<<" \t ctagbin" <<jetTag[jetRank[1]]<<" \t ctagbin"<<jetTag[jetRank[2]]<<std::endl;
        }
        //////////////////////////////////////////////
        //  Apply Jet ReRank
        /////////////////////////
        if (ApplyReRank){
            if ( nJ > 2 ){
                for (int i=0;i<2;i++){
                    for (int j=i+1;j<3;j++){
                        if (jetTag[i] < jetTag[j]){
                            if (j==2)std::cout<<" rerank by jet tag "<<i<<" "<<j<<std::endl;
                            int tmpTag=jetTag[i];
                            jetTag[i]=jetTag[j];
                            jetTag[j]=tmpTag;
                            int tmpRank=jetRank[i];
                            jetRank[i]=jetRank[j];
                            jetRank[j]=tmpRank;
                        }
                    }
                }
            }else{
                if (bin_bTagB1 < bin_bTagB2){
                    jetRank[0]=1 ;
                    jetRank[1]=0 ;
                }
            }
        }

        Int_t m_Flav[3]={FlavB1,FlavB2,FlavJ3};
        Float_t m_pT[3]={pTB1,pTB2, pTJ3};
        Float_t m_eta[3]={etaB1,etaB2, etaJ3};
        Float_t m_phi[3]={phiB1,phiB2, phiJ3};
        Float_t m_m[3]={mB1,mB2, mJ3};
        Float_t m_bin_bTag[3]={bin_bTagB1,bin_bTagB2, bin_bTagJ3};
        Float_t m_DL1r_pb[3]={DL1r_pb_B1,DL1r_pb_B2, DL1r_pb_J3};
        Float_t m_DL1r_pc[3]={DL1r_pc_B1,DL1r_pc_B2, DL1r_pc_J3};
        Float_t m_DL1r_pu[3]={DL1r_pu_B1,DL1r_pu_B2, DL1r_pu_J3};
        //////////////////////////////////////////////
        // Rerank by Jet Pt 
        /////////////////////////
        if (ApplyPtRank){
            if (m_pT[jetRank[0]]< m_pT[jetRank[1]]) {
                //       std::cout<<" rerank by jet pT "<<std::endl;
                int tmpRank=jetRank[1];
                jetRank[0]=jetRank[1];
                jetRank[1]=tmpRank;
            }
        }

        // new value 
        m_pTB1  = m_pT[jetRank[0]],  m_pTB2 = m_pT[jetRank[1]],   m_pTJ3 = m_pT[jetRank[2]];
        m_etaB1 = m_eta[jetRank[0]], m_etaB2 = m_eta[jetRank[1]], m_etaJ3 = m_eta[jetRank[2]];
        m_phiB1 = m_phi[jetRank[0]], m_phiB2 = m_phi[jetRank[1]], m_phiJ3 = m_phi[jetRank[2]];
        m_mB1  = m_m[jetRank[0]],  m_mB2 = m_m[jetRank[1]],   m_mJ3 = m_m[jetRank[2]];
        m_bin_bTagB1  = m_bin_bTag[jetRank[0]], m_bin_bTagB2 = m_bin_bTag[jetRank[1]],  m_bin_bTagJ3 = m_bin_bTag[jetRank[2]];
        m_DL1r_pb_B1  = m_DL1r_pb[jetRank[0]],  m_DL1r_pb_B2 = m_DL1r_pb[jetRank[1]],   m_DL1r_pb_J3 = m_DL1r_pb[jetRank[2]];
        m_DL1r_pu_B1  = m_DL1r_pu[jetRank[0]],  m_DL1r_pu_B2 = m_DL1r_pu[jetRank[1]],   m_DL1r_pu_J3 = m_DL1r_pu[jetRank[2]];
        m_DL1r_pc_B1  = m_DL1r_pc[jetRank[0]],  m_DL1r_pc_B2 = m_DL1r_pc[jetRank[1]],   m_DL1r_pc_J3 = m_DL1r_pc[jetRank[2]];
        m_FlavB1  = m_Flav[jetRank[0]],  m_FlavB2 = m_Flav[jetRank[1]],   m_FlavJ3 = m_Flav[jetRank[2]];
        //TLorentzVector 
        m_Jet[0].SetPtEtaPhiM(m_pTB1,m_etaB1,m_phiB1,m_mB1);
        m_Jet[1].SetPtEtaPhiM(m_pTB2,m_etaB2,m_phiB2,m_mB2);
        m_H = m_Jet[0]+m_Jet[1];
        m_V.SetPtEtaPhiM(pTV,etaV,phiV,mLL);
        m_dRBB= m_Jet[0].DeltaR(m_Jet[1]);
        m_mBB = m_H.M();
        m_dPhiBB= fabs(m_Jet[0].DeltaPhi(m_Jet[1]));
        m_dEtaBB= fabs(m_Jet[0].Eta()-m_Jet[1].Eta());
        m_dPhiVBB = fabs(m_V.DeltaPhi(m_H));
        m_dEtaVBB = fabs(m_V.Eta()-m_H.Eta());
        m_dRVBB = m_V.DeltaR(m_H);
        //////////////////////////////////////////////
        // dRCut for SR  
        /////////////////////////
        PassSelection_dR = passSRCut_VHcc(pTV,m_dRBB);
        PassSelection_Previous *= PassSelection_dR;
        if (PassSelection_Previous){
            h_cutFlow->Fill(4.5);
            h_cutFlow_weight->Fill(4.5,EventWeight/bTagWeight);
            if (nJ<3) {
                h_cutFlow_2J->Fill(4.5);
                h_cutFlow_2J_weight->Fill(4.5,EventWeight/bTagWeight);
            }else {
                h_cutFlow_3J->Fill(4.5);
                h_cutFlow_3J_weight->Fill(4.5,EventWeight/bTagWeight);
            }
        }

        if (counter %100000== 0 && ApplyReRank) 
        {   
            std::cout<<" -------- After rank -------------"<<std::endl;
            std::cout<<"After"<< " \t Jet"<<jetRank[0]<<" \t Jet" <<jetRank[1]<<" \t Jet"<<jetRank[2]<<std::endl;
            std::cout<<"After"<< " \t ctagbin"<<jetTag[jetRank[0]]<<" \t ctagbin" <<jetTag[jetRank[1]]<<" \t ctagbin"<<jetTag[jetRank[2]]<<std::endl;
            std::cout<<"Before"<< " \t dEtaVBB "<<dEtaVBB<<" \t dEtaBB " <<dEtaBB<<" \t dPhiVBB "<<dPhiVBB<<" \t dPhiBB " <<dPhiBB<<" \t mBB "<< mBB <<std::endl;
            std::cout<<"After "<< " \t dEtaVBB "<<m_dEtaVBB<<" \t dEtaBB " <<m_dEtaBB<<" \t dPhiVBB "<<m_dPhiVBB<<" \t dPhiBB " <<m_dPhiBB<<" \t mBB "<< m_mBB <<std::endl;
            counter+=1;
        }
        int  PassSelection = PassSelection_SF * PassSelection_BVeto;
        if (slimNtuple) PassSelection *= PassSelection_Slim;
        if (ApplyDRCut) PassSelection *= PassSelection_dR;
        if (PassSelection > 0) {
            counter+=1;
            MVATree->Fill();
        }
    }
    h_cutFlow->Write();
    h_cutFlow_2J->Write();
    h_cutFlow_3J->Write();
    h_cutFlow_weight->Write();
    h_cutFlow_2J_weight->Write();
    h_cutFlow_3J_weight->Write();
    h_CtagCheck->Write();
    h_CtagCheck_JetEta->Write();
    h_CtagCheck_JetEtaB1->Write();
    h_CtagCheck_JetEtaB2->Write();
    h_CtagCheck_JetEtaJ3->Write();

    h_nJnSigJ->Write();
    h_CtagCheck_JetpTB1->Write();
    h_CtagCheck_JetpTB2->Write();
    h_CtagCheck_JetpTJ3->Write();
    h_CtagCheck_JetNum->Write();
    MVATree->Write();
    f_new.Write();
    f_new.Close();

}
