//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon May 23 15:49:37 2022 by ROOT version 6.20/08
// from TTree Nominal/Nominal
// found on file: tree-VHcc.root
//////////////////////////////////////////////////////////

#ifndef JetReRank_h
#define JetReRank_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "string"

class JetReRank {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   string          *sample;
   Float_t         EventWeight;
   Float_t         bTagWeight;
   ULong64_t       EventNumber;
   Int_t           ChannelNumber;
   string          *EventFlavor;
   Float_t         FlavourLabel;
   string          *Description;
   Int_t           isResolved;
   Int_t           isBoosted;
   Int_t           nJ;
   Int_t           nSigJet;
   Int_t           nForwardJet;
   Int_t           nTags;
   Int_t           nTaus;
   Float_t         MET;
   Float_t         MEff;
   Float_t         MEffBoosted;
   Float_t         METSig;
   Float_t         dPhiVBB;
   Float_t         dEtaVBB;
   Float_t         sumPtJets;
   Float_t         softMET;
   Float_t         hasFSR;
   Float_t         nFatJets;
   Float_t         NMatchedTrackJetLeadFatJet;
   Float_t         NBTagMatchedTrackJetLeadFatJet;
   Float_t         NBTagUnmatchedTrackJetLeadFatJet;
   Float_t         NAdditionalCaloJets;
   Float_t         pTAddCaloJets;
   Float_t         deltaYVJ;
   Float_t         absdeltaPhiVJ;
   Float_t         HTBoosted;
   Float_t         pTV;
   Float_t         phiV;
   Float_t         dRBB;
   Float_t         dPhiBB;
   Float_t         dEtaBB;
   Float_t         mBB;
   Float_t         mBBJ;
   Float_t         mJ;
   Float_t         pTJ;
   Float_t         deltaRbTrkJbTrkJ;
   Float_t         D2;
   Float_t         C2;
   Float_t         deltaRFJTrkJ1;
   Float_t         deltaRFJTrkJ2;
   Float_t         deltaRFJTrkJ3;
   Float_t         deltaRbTrkJ1TrkJ3;
   Float_t         pTB1;
   Float_t         pTB2;
   Float_t         pTJ3;
   Float_t         pTBTrkJ1;
   Float_t         pTBTrkJ2;
   Float_t         pTBTrkJ3;
   Float_t         etaBTrkJ1;
   Float_t         etaBTrkJ2;
   Float_t         etaBTrkJ3;
   Float_t         phiBTrkJ1;
   Float_t         phiBTrkJ2;
   Float_t         phiBTrkJ3;
   Float_t         etaB1;
   Float_t         etaB2;
   Float_t         etaJ3;
   Float_t         phiB1;
   Float_t         phiB2;
   Float_t         phiJ3;
   Float_t         mB1;
   Float_t         mB2;
   Float_t         mJ3;
   Float_t         bin_bTagB1;
   Float_t         bin_bTagB2;
   Float_t         bin_bTagJ3;
   Float_t         bin_bTagBTrkJ1;
   Float_t         bin_bTagBTrkJ2;
   Float_t         bin_bTagBTrkJ3;
   Float_t         Xbb202006;
   Float_t         DL1r_pb_B1;
   Float_t         DL1r_pc_B1;
   Float_t         DL1r_pu_B1;
   Float_t         DL1r_pb_B2;
   Float_t         DL1r_pc_B2;
   Float_t         DL1r_pu_B2;
   Float_t         DL1r_pb_J3;
   Float_t         DL1r_pc_J3;
   Float_t         DL1r_pu_J3;
   Float_t         DL1r_pb_TrkJ1;
   Float_t         DL1r_pc_TrkJ1;
   Float_t         DL1r_pu_TrkJ1;
   Float_t         DL1r_pb_TrkJ2;
   Float_t         DL1r_pc_TrkJ2;
   Float_t         DL1r_pu_TrkJ2;
   Float_t         DL1r_pb_TrkJ3;
   Float_t         DL1r_pc_TrkJ3;
   Float_t         DL1r_pu_TrkJ3;
   Int_t           nTagJet_4BVeto;
   Int_t           FlavB1;
   Int_t           FlavB2;
   Int_t           FlavJ3;
   Int_t           FlavTrkJ1;
   Int_t           FlavTrkJ2;
   Int_t           FlavTrkJ3;
   Int_t           FlavL1;
   Int_t           FlavL2;
   Float_t         mLL;
   Float_t         etaV;
   Float_t         pTL1;
   Float_t         pTL2;
   Float_t         etaL1;
   Float_t         etaL2;
   Float_t         phiL1;
   Float_t         phiL2;
   Float_t         cosThetaLep;
   Float_t         lepPtBalance;
   Float_t         BDT;
   Float_t         BDT_VZ;
   Float_t         BDTBoosted;

   // List of branches
   TBranch        *b_sample;   //!
   TBranch        *b_EventWeight;   //!
   TBranch        *b_bTagWeight;   //!
   TBranch        *b_EventNumber;   //!
   TBranch        *b_ChannelNumber;   //!
   TBranch        *b_EventFlavor;   //!
   TBranch        *b_FlavourLabel;   //!
   TBranch        *b_Description;   //!
   TBranch        *b_isResolved;   //!
   TBranch        *b_isBoosted;   //!
   TBranch        *b_nJ;   //!
   TBranch        *b_nSigJet;   //!
   TBranch        *b_nForwardJet;   //!
   TBranch        *b_nTags;   //!
   TBranch        *b_nTaus;   //!
   TBranch        *b_MET;   //!
   TBranch        *b_MEff;   //!
   TBranch        *b_MEffBoosted;   //!
   TBranch        *b_METSig;   //!
   TBranch        *b_dPhiVBB;   //!
   TBranch        *b_dEtaVBB;   //!
   TBranch        *b_sumPtJets;   //!
   TBranch        *b_softMET;   //!
   TBranch        *b_hasFSR;   //!
   TBranch        *b_nFatJets;   //!
   TBranch        *b_NMatchedTrackJetLeadFatJet;   //!
   TBranch        *b_NBTagMatchedTrackJetLeadFatJet;   //!
   TBranch        *b_NBTagUnmatchedTrackJetLeadFatJet;   //!
   TBranch        *b_NAdditionalCaloJets;   //!
   TBranch        *b_pTAddCaloJets;   //!
   TBranch        *b_deltaYVJ;   //!
   TBranch        *b_absdeltaPhiVJ;   //!
   TBranch        *b_HTBoosted;   //!
   TBranch        *b_pTV;   //!
   TBranch        *b_phiV;   //!
   TBranch        *b_dRBB;   //!
   TBranch        *b_dPhiBB;   //!
   TBranch        *b_dEtaBB;   //!
   TBranch        *b_mBB;   //!
   TBranch        *b_mBBJ;   //!
   TBranch        *b_mJ;   //!
   TBranch        *b_pTJ;   //!
   TBranch        *b_deltaRbTrkJbTrkJ;   //!
   TBranch        *b_D2;   //!
   TBranch        *b_C2;   //!
   TBranch        *b_deltaRFJTrkJ1;   //!
   TBranch        *b_deltaRFJTrkJ2;   //!
   TBranch        *b_deltaRFJTrkJ3;   //!
   TBranch        *b_deltaRbTrkJ1TrkJ3;   //!
   TBranch        *b_pTB1;   //!
   TBranch        *b_pTB2;   //!
   TBranch        *b_pTJ3;   //!
   TBranch        *b_pTBTrkJ1;   //!
   TBranch        *b_pTBTrkJ2;   //!
   TBranch        *b_pTBTrkJ3;   //!
   TBranch        *b_etaBTrkJ1;   //!
   TBranch        *b_etaBTrkJ2;   //!
   TBranch        *b_etaBTrkJ3;   //!
   TBranch        *b_phiBTrkJ1;   //!
   TBranch        *b_phiBTrkJ2;   //!
   TBranch        *b_phiBTrkJ3;   //!
   TBranch        *b_etaB1;   //!
   TBranch        *b_etaB2;   //!
   TBranch        *b_etaJ3;   //!
   TBranch        *b_phiB1;   //!
   TBranch        *b_phiB2;   //!
   TBranch        *b_phiJ3;   //!
   TBranch        *b_mB1;   //!
   TBranch        *b_mB2;   //!
   TBranch        *b_mJ3;   //!
   TBranch        *b_bin_bTagB1;   //!
   TBranch        *b_bin_bTagB2;   //!
   TBranch        *b_bin_bTagJ3;   //!
   TBranch        *b_bin_bTagBTrkJ1;   //!
   TBranch        *b_bin_bTagBTrkJ2;   //!
   TBranch        *b_bin_bTagBTrkJ3;   //!
   TBranch        *b_Xbb202006;   //!
   TBranch        *b_DL1r_pb_B1;   //!
   TBranch        *b_DL1r_pc_B1;   //!
   TBranch        *b_DL1r_pu_B1;   //!
   TBranch        *b_DL1r_pb_B2;   //!
   TBranch        *b_DL1r_pc_B2;   //!
   TBranch        *b_DL1r_pu_B2;   //!
   TBranch        *b_DL1r_pb_J3;   //!
   TBranch        *b_DL1r_pc_J3;   //!
   TBranch        *b_DL1r_pu_J3;   //!
   TBranch        *b_DL1r_pb_TrkJ1;   //!
   TBranch        *b_DL1r_pc_TrkJ1;   //!
   TBranch        *b_DL1r_pu_TrkJ1;   //!
   TBranch        *b_DL1r_pb_TrkJ2;   //!
   TBranch        *b_DL1r_pc_TrkJ2;   //!
   TBranch        *b_DL1r_pu_TrkJ2;   //!
   TBranch        *b_DL1r_pb_TrkJ3;   //!
   TBranch        *b_DL1r_pc_TrkJ3;   //!
   TBranch        *b_DL1r_pu_TrkJ3;   //!
   TBranch        *b_nTagJet_4BVeto;   //!
   TBranch        *b_FlavB1;   //!
   TBranch        *b_FlavB2;   //!
   TBranch        *b_FlavJ3;   //!
   TBranch        *b_FlavTrkJ1;   //!
   TBranch        *b_FlavTrkJ2;   //!
   TBranch        *b_FlavTrkJ3;   //!
   TBranch        *b_FlavL1;   //!
   TBranch        *b_FlavL2;   //!
   TBranch        *b_mLL;   //!
   TBranch        *b_etaV;   //!
   TBranch        *b_pTL1;   //!
   TBranch        *b_pTL2;   //!
   TBranch        *b_etaL1;   //!
   TBranch        *b_etaL2;   //!
   TBranch        *b_phiL1;   //!
   TBranch        *b_phiL2;   //!
   TBranch        *b_cosThetaLep;   //!
   TBranch        *b_lepPtBalance;   //!
   TBranch        *b_BDT;   //!
   TBranch        *b_BDT_VZ;   //!
   TBranch        *b_BDTBoosted;   //!

   JetReRank(TTree *tree=0);
   virtual ~JetReRank();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop(string outputName, bool slimNtuple, bool ApplyReRank = true, bool ApplyPtRank = false , bool ApplyDRCut = false);
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef JetReRank_cxx
JetReRank::JetReRank(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("tree-VHcc.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("tree-VHcc.root");
      }
      f->GetObject("Nominal",tree);

   }
   Init(tree);
}

JetReRank::~JetReRank()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t JetReRank::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t JetReRank::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void JetReRank::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   sample = 0;
   EventFlavor = 0;
   Description = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("sample", &sample, &b_sample);
   fChain->SetBranchAddress("EventWeight", &EventWeight, &b_EventWeight);
   fChain->SetBranchAddress("bTagWeight", &bTagWeight, &b_bTagWeight);
   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   fChain->SetBranchAddress("ChannelNumber", &ChannelNumber, &b_ChannelNumber);
   fChain->SetBranchAddress("EventFlavor", &EventFlavor, &b_EventFlavor);
   fChain->SetBranchAddress("FlavourLabel", &FlavourLabel, &b_FlavourLabel);
   fChain->SetBranchAddress("Description", &Description, &b_Description);
   fChain->SetBranchAddress("isResolved", &isResolved, &b_isResolved);
   fChain->SetBranchAddress("isBoosted", &isBoosted, &b_isBoosted);
   fChain->SetBranchAddress("nJ", &nJ, &b_nJ);
   fChain->SetBranchAddress("nSigJet", &nSigJet, &b_nSigJet);
   fChain->SetBranchAddress("nForwardJet", &nForwardJet, &b_nForwardJet);
   fChain->SetBranchAddress("nTags", &nTags, &b_nTags);
   fChain->SetBranchAddress("nTaus", &nTaus, &b_nTaus);
   fChain->SetBranchAddress("MET", &MET, &b_MET);
   fChain->SetBranchAddress("MEff", &MEff, &b_MEff);
   fChain->SetBranchAddress("MEffBoosted", &MEffBoosted, &b_MEffBoosted);
   fChain->SetBranchAddress("METSig", &METSig, &b_METSig);
   fChain->SetBranchAddress("dPhiVBB", &dPhiVBB, &b_dPhiVBB);
   fChain->SetBranchAddress("dEtaVBB", &dEtaVBB, &b_dEtaVBB);
   fChain->SetBranchAddress("sumPtJets", &sumPtJets, &b_sumPtJets);
   fChain->SetBranchAddress("softMET", &softMET, &b_softMET);
   fChain->SetBranchAddress("hasFSR", &hasFSR, &b_hasFSR);
   fChain->SetBranchAddress("nFatJets", &nFatJets, &b_nFatJets);
   fChain->SetBranchAddress("NMatchedTrackJetLeadFatJet", &NMatchedTrackJetLeadFatJet, &b_NMatchedTrackJetLeadFatJet);
   fChain->SetBranchAddress("NBTagMatchedTrackJetLeadFatJet", &NBTagMatchedTrackJetLeadFatJet, &b_NBTagMatchedTrackJetLeadFatJet);
   fChain->SetBranchAddress("NBTagUnmatchedTrackJetLeadFatJet", &NBTagUnmatchedTrackJetLeadFatJet, &b_NBTagUnmatchedTrackJetLeadFatJet);
   fChain->SetBranchAddress("NAdditionalCaloJets", &NAdditionalCaloJets, &b_NAdditionalCaloJets);
   fChain->SetBranchAddress("pTAddCaloJets", &pTAddCaloJets, &b_pTAddCaloJets);
   fChain->SetBranchAddress("deltaYVJ", &deltaYVJ, &b_deltaYVJ);
   fChain->SetBranchAddress("absdeltaPhiVJ", &absdeltaPhiVJ, &b_absdeltaPhiVJ);
   fChain->SetBranchAddress("HTBoosted", &HTBoosted, &b_HTBoosted);
   fChain->SetBranchAddress("pTV", &pTV, &b_pTV);
   fChain->SetBranchAddress("phiV", &phiV, &b_phiV);
   fChain->SetBranchAddress("dRBB", &dRBB, &b_dRBB);
   fChain->SetBranchAddress("dPhiBB", &dPhiBB, &b_dPhiBB);
   fChain->SetBranchAddress("dEtaBB", &dEtaBB, &b_dEtaBB);
   fChain->SetBranchAddress("mBB", &mBB, &b_mBB);
   fChain->SetBranchAddress("mBBJ", &mBBJ, &b_mBBJ);
   fChain->SetBranchAddress("mJ", &mJ, &b_mJ);
   fChain->SetBranchAddress("pTJ", &pTJ, &b_pTJ);
   fChain->SetBranchAddress("deltaRbTrkJbTrkJ", &deltaRbTrkJbTrkJ, &b_deltaRbTrkJbTrkJ);
   fChain->SetBranchAddress("D2", &D2, &b_D2);
   fChain->SetBranchAddress("C2", &C2, &b_C2);
   fChain->SetBranchAddress("deltaRFJTrkJ1", &deltaRFJTrkJ1, &b_deltaRFJTrkJ1);
   fChain->SetBranchAddress("deltaRFJTrkJ2", &deltaRFJTrkJ2, &b_deltaRFJTrkJ2);
   fChain->SetBranchAddress("deltaRFJTrkJ3", &deltaRFJTrkJ3, &b_deltaRFJTrkJ3);
   fChain->SetBranchAddress("deltaRbTrkJ1TrkJ3", &deltaRbTrkJ1TrkJ3, &b_deltaRbTrkJ1TrkJ3);
   fChain->SetBranchAddress("pTB1", &pTB1, &b_pTB1);
   fChain->SetBranchAddress("pTB2", &pTB2, &b_pTB2);
   fChain->SetBranchAddress("pTJ3", &pTJ3, &b_pTJ3);
   fChain->SetBranchAddress("pTBTrkJ1", &pTBTrkJ1, &b_pTBTrkJ1);
   fChain->SetBranchAddress("pTBTrkJ2", &pTBTrkJ2, &b_pTBTrkJ2);
   fChain->SetBranchAddress("pTBTrkJ3", &pTBTrkJ3, &b_pTBTrkJ3);
   fChain->SetBranchAddress("etaBTrkJ1", &etaBTrkJ1, &b_etaBTrkJ1);
   fChain->SetBranchAddress("etaBTrkJ2", &etaBTrkJ2, &b_etaBTrkJ2);
   fChain->SetBranchAddress("etaBTrkJ3", &etaBTrkJ3, &b_etaBTrkJ3);
   fChain->SetBranchAddress("phiBTrkJ1", &phiBTrkJ1, &b_phiBTrkJ1);
   fChain->SetBranchAddress("phiBTrkJ2", &phiBTrkJ2, &b_phiBTrkJ2);
   fChain->SetBranchAddress("phiBTrkJ3", &phiBTrkJ3, &b_phiBTrkJ3);
   fChain->SetBranchAddress("etaB1", &etaB1, &b_etaB1);
   fChain->SetBranchAddress("etaB2", &etaB2, &b_etaB2);
   fChain->SetBranchAddress("etaJ3", &etaJ3, &b_etaJ3);
   fChain->SetBranchAddress("phiB1", &phiB1, &b_phiB1);
   fChain->SetBranchAddress("phiB2", &phiB2, &b_phiB2);
   fChain->SetBranchAddress("phiJ3", &phiJ3, &b_phiJ3);
   fChain->SetBranchAddress("mB1", &mB1, &b_mB1);
   fChain->SetBranchAddress("mB2", &mB2, &b_mB2);
   fChain->SetBranchAddress("mJ3", &mJ3, &b_mJ3);
   fChain->SetBranchAddress("bin_bTagB1", &bin_bTagB1, &b_bin_bTagB1);
   fChain->SetBranchAddress("bin_bTagB2", &bin_bTagB2, &b_bin_bTagB2);
   fChain->SetBranchAddress("bin_bTagJ3", &bin_bTagJ3, &b_bin_bTagJ3);
   fChain->SetBranchAddress("bin_bTagBTrkJ1", &bin_bTagBTrkJ1, &b_bin_bTagBTrkJ1);
   fChain->SetBranchAddress("bin_bTagBTrkJ2", &bin_bTagBTrkJ2, &b_bin_bTagBTrkJ2);
   fChain->SetBranchAddress("bin_bTagBTrkJ3", &bin_bTagBTrkJ3, &b_bin_bTagBTrkJ3);
   fChain->SetBranchAddress("Xbb202006", &Xbb202006, &b_Xbb202006);
   fChain->SetBranchAddress("DL1r_pb_B1", &DL1r_pb_B1, &b_DL1r_pb_B1);
   fChain->SetBranchAddress("DL1r_pc_B1", &DL1r_pc_B1, &b_DL1r_pc_B1);
   fChain->SetBranchAddress("DL1r_pu_B1", &DL1r_pu_B1, &b_DL1r_pu_B1);
   fChain->SetBranchAddress("DL1r_pb_B2", &DL1r_pb_B2, &b_DL1r_pb_B2);
   fChain->SetBranchAddress("DL1r_pc_B2", &DL1r_pc_B2, &b_DL1r_pc_B2);
   fChain->SetBranchAddress("DL1r_pu_B2", &DL1r_pu_B2, &b_DL1r_pu_B2);
   fChain->SetBranchAddress("DL1r_pb_J3", &DL1r_pb_J3, &b_DL1r_pb_J3);
   fChain->SetBranchAddress("DL1r_pc_J3", &DL1r_pc_J3, &b_DL1r_pc_J3);
   fChain->SetBranchAddress("DL1r_pu_J3", &DL1r_pu_J3, &b_DL1r_pu_J3);
   fChain->SetBranchAddress("DL1r_pb_TrkJ1", &DL1r_pb_TrkJ1, &b_DL1r_pb_TrkJ1);
   fChain->SetBranchAddress("DL1r_pc_TrkJ1", &DL1r_pc_TrkJ1, &b_DL1r_pc_TrkJ1);
   fChain->SetBranchAddress("DL1r_pu_TrkJ1", &DL1r_pu_TrkJ1, &b_DL1r_pu_TrkJ1);
   fChain->SetBranchAddress("DL1r_pb_TrkJ2", &DL1r_pb_TrkJ2, &b_DL1r_pb_TrkJ2);
   fChain->SetBranchAddress("DL1r_pc_TrkJ2", &DL1r_pc_TrkJ2, &b_DL1r_pc_TrkJ2);
   fChain->SetBranchAddress("DL1r_pu_TrkJ2", &DL1r_pu_TrkJ2, &b_DL1r_pu_TrkJ2);
   fChain->SetBranchAddress("DL1r_pb_TrkJ3", &DL1r_pb_TrkJ3, &b_DL1r_pb_TrkJ3);
   fChain->SetBranchAddress("DL1r_pc_TrkJ3", &DL1r_pc_TrkJ3, &b_DL1r_pc_TrkJ3);
   fChain->SetBranchAddress("DL1r_pu_TrkJ3", &DL1r_pu_TrkJ3, &b_DL1r_pu_TrkJ3);
   fChain->SetBranchAddress("nTagJet_4BVeto", &nTagJet_4BVeto, &b_nTagJet_4BVeto);
   fChain->SetBranchAddress("FlavB1", &FlavB1, &b_FlavB1);
   fChain->SetBranchAddress("FlavB2", &FlavB2, &b_FlavB2);
   fChain->SetBranchAddress("FlavJ3", &FlavJ3, &b_FlavJ3);
   fChain->SetBranchAddress("FlavTrkJ1", &FlavTrkJ1, &b_FlavTrkJ1);
   fChain->SetBranchAddress("FlavTrkJ2", &FlavTrkJ2, &b_FlavTrkJ2);
   fChain->SetBranchAddress("FlavTrkJ3", &FlavTrkJ3, &b_FlavTrkJ3);
   fChain->SetBranchAddress("FlavL1", &FlavL1, &b_FlavL1);
   fChain->SetBranchAddress("FlavL2", &FlavL2, &b_FlavL2);
   fChain->SetBranchAddress("mLL", &mLL, &b_mLL);
   fChain->SetBranchAddress("etaV", &etaV, &b_etaV);
   fChain->SetBranchAddress("pTL1", &pTL1, &b_pTL1);
   fChain->SetBranchAddress("pTL2", &pTL2, &b_pTL2);
   fChain->SetBranchAddress("etaL1", &etaL1, &b_etaL1);
   fChain->SetBranchAddress("etaL2", &etaL2, &b_etaL2);
   fChain->SetBranchAddress("phiL1", &phiL1, &b_phiL1);
   fChain->SetBranchAddress("phiL2", &phiL2, &b_phiL2);
   fChain->SetBranchAddress("cosThetaLep", &cosThetaLep, &b_cosThetaLep);
   fChain->SetBranchAddress("lepPtBalance", &lepPtBalance, &b_lepPtBalance);
   fChain->SetBranchAddress("BDT", &BDT, &b_BDT);
   fChain->SetBranchAddress("BDT_VZ", &BDT_VZ, &b_BDT_VZ);
   fChain->SetBranchAddress("BDTBoosted", &BDTBoosted, &b_BDTBoosted);
   Notify();
}

Bool_t JetReRank::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void JetReRank::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t JetReRank::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef JetReRank_cxx
